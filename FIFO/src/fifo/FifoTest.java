package fifo;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FifoTest {
    private FIFO cola;
	@Before
	public void setUp() throws Exception {
		cola = new FIFO();		
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void testConstructor() {
	    assertEquals("fifo.FIFO", cola.getClass().getName().toString());
	}
	
	@Test
	public void testLongitud() {
		assertEquals(0, cola.longitud());
	}

	 @Test
	 public void testLongitudTrasPoner() {
		 cola.poner(8);
		 assertEquals(1, cola.longitud()); 
	 }

	 @Test
	 public void testLongitudTrasPoner2() {
		 cola.poner(8);
		 cola.poner(7);
		 assertEquals(2, cola.longitud()); 
	 }
	 
	 @Test
	 public void testVer() {
		 FIFO cola = new FIFO();
		 cola.poner(8);
		 assertEquals(8, cola.verProximo());
	 }
	 @Test
	 public void testVerOtro() {
		 FIFO cola = new FIFO();
		 cola.poner(9);
		 assertEquals(9, cola.verProximo());
	 }
	 @Test
	 public void testVerTrasPonerDos() {
		 cola.poner(9);
		 cola.poner(3);
		 assertEquals(9, cola.verProximo());
	 }	 
	 @Test
	 public void testLongitudTrasSacar() {
		 cola.poner(7);
		 cola.sacar();
		 assertEquals(0, cola.longitud()); 
	 }

	 @Test
	 public void testSacar() {
		 cola.poner(7);
		 assertEquals(7, cola.sacar());
	 }

	 @Test
	 public void testSacarTrasDos() {
		 cola.poner(7);
		 cola.poner(5);
		 assertEquals(7, cola.sacar());
	 }

	 @Test
	 public void testSacarDosTrasDos() {
		 cola.poner(7);
		 cola.poner(5);
		 cola.sacar();
		 assertEquals(5, cola.sacar());
	 }
	 
}
