package cliente;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RenaperTest {

	private MockRenaper renaper;
	
	@Before
	public void setUp() throws Exception {
		this.renaper = new MockRenaper();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBuscarDniDe22333666() {
		assertEquals("Martin", this.renaper.buscarDni("22333666"));
	}
	
	@Test
	public void testBuscarDniDe22333777() {
		assertEquals("Mirta", this.renaper.buscarDni("22333777"));
	}
}
