package cliente;

public class Cliente {
	
   private String dni;
   private String nombre;
   
   static public Cliente  ClienteFactory(String dni, String tipo) {
	 if (tipo == "Mock") return new Cliente(dni, new MockRenaper());
	 return new Cliente(dni);
   }

   private Cliente(String dni, DatosRenaper fuente) {
	   this.dni = dni;
	   this.nombre = fuente.buscarDni(dni);
   }
   
   public Cliente(String dni) {
	   this.dni = dni;
   }
   
   public String getDNI() {
	   return this.dni;
   }

    public void setNombre(String nombre) {
	   this.nombre = nombre;
   }
    
    public String getNombre() {
    	return this.nombre;
    }
}
