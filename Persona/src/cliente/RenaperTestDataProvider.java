package cliente;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


@RunWith(Parameterized.class)
public class RenaperTestDataProvider {

	private MockRenaper renaper;
	
	@Before
	public void setUp() throws Exception {
		this.renaper = new MockRenaper();
	}

    @Parameters
    public static Collection<Object[]>data() {
    	return Arrays.asList(new Object[][] {
    		{"22333666","Martin"},
    		{"22333777","Mirta"}
    		
    	});
    }
    
    private String given;
    private String expected;
	
    public RenaperTestDataProvider(String given, String expected) {
    	this.given = given;
    	this.expected = expected;
    }
    
	@Test
	public void testBuscarDni() {
		assertEquals(this.expected, this.renaper.buscarDni(this.given));
	}
}
