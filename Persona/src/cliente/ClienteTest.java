package cliente;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ClienteTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConstructor() {
		Cliente cliente = new Cliente("22333444");
	    assertEquals("22333444", cliente.getDNI());	
	}
	
	@Test
	public void testGetDNI() {
		Cliente cliente = new Cliente("22333555");
	    assertEquals("22333555", cliente.getDNI());	
	}
	
	@Test
	public void testGetNombre() {
		Cliente cliente = new Cliente("22333555");
		cliente.setNombre("Martina");
	    assertEquals("Martina", cliente.getNombre());
	}

	@Test
	public void testCargarNombreEnConstructor() {
		Cliente cliente = Cliente.ClienteFactory("22333666", "Mock");
	    assertEquals("Martin", cliente.getNombre());	
	}
	
	@Test
	public void testCargaroOtroNombreEnConstructor() {
		Cliente cliente = Cliente.ClienteFactory("22333777", "Mock");
	    assertEquals("Mirta", cliente.getNombre());	
	}	

}
